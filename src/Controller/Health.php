<?php

declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class Health extends Controller
{
    public function status()
    {
        return new Response('', 200, ['pass' => true]);
    }
}
