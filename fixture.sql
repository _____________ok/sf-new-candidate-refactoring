CREATE DATABASE mail;

CREATE TABLE mail.mail_queue
(
    id int(11),
    mail varchar(123),
    created_at datetime,
    status text,
    receiver text,
    email text,
    subject text,
    types text
);
